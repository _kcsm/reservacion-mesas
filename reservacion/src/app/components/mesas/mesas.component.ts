import { Component, OnInit } from '@angular/core';
import * as SVG from 'svg.js';
import { MesasService } from '../../services/mesas.service';
import { Console } from '@angular/core/src/console';



@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.component.html',
  styles: []
})
export class MesasComponent implements OnInit {
  svgCanvas:any;
  public  mesas2=[];
        // Representa las mesas y las sillas del salón con sus atributos
        mesas = [
            {
                "color": "#4C1E3C",
                "nombre": "vacia",
                "posicion": {
                    "x": 50,
                    "y": 50
                },
                "sillas": [
                    {
                        "color": "#8171BE",
                        "numero": 1,
                        "ocupada": false,
                        "posicion": {
                            "x": 30,
                            "y": 50
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 2,
                        "ocupada": false,
                        "posicion": {
                            "x": 100,
                            "y": 10
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 3,
                        "ocupada": false,
                        "posicion": {
                            "x": 180,
                            "y": 10
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 4,
                        "ocupada": false,
                        "posicion": {
                            "x": 240,
                            "y": 50
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 5,
                        "ocupada": false,
                        "posicion": {
                            "x": 260,
                            "y": 130
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 6,
                        "ocupada": false,
                        "posicion": {
                            "x": 240,
                            "y": 200
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 7,
                        "ocupada": false,
                        "posicion": {
                            "x": 205,
                            "y": 255
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 8,
                        "ocupada": false,
                        "posicion": {
                            "x": 118,
                            "y": 270
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 9,
                        "ocupada": false,
                        "posicion": {
                            "x": 35,
                            "y": 230
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 10,
                        "ocupada": false,
                        "posicion": {
                            "x": 5,
                            "y": 145
                        }
                    }   
                ]
            },
        
            {
                "color": "#4C1E3C",
                "nombre": "vacia",
                "posicion": {
                    "x": 500,
                    "y": 50
                },
                "sillas": [
                    {
                        "color": "#8171BE",
                        "numero": 1,
                        "ocupada": false,
                        "posicion": {
                            "x": 579,
                            "y": 4
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 2,
                        "ocupada": false,
                        "posicion": {
                            "x": 671,
                            "y": 41
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 3,
                        "ocupada": false,
                        "posicion": {
                            "x": 708,
                            "y": 112
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 4,
                        "ocupada": false,
                        "posicion": {
                            "x": 704,
                            "y": 179
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 5,
                        "ocupada": false,
                        "posicion": {
                            "x": 664,
                            "y": 229
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 6,
                        "ocupada": false,
                        "posicion": {
                            "x": 583,
                            "y": 253
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 7,
                        "ocupada": false,
                        "posicion": {
                            "x": 499,
                            "y": 229
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 8,
                        "ocupada": false,
                        "posicion": {
                            "x": 459,
                            "y": 164
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 9,
                        "ocupada": false,
                        "posicion": {
                            "x": 499,
                            "y": 29
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 10,
                        "ocupada": false,
                        "posicion": {
                            "x": 459,
                            "y": 94
                        }
                    }
                ]
            },
        
            {
                "color": "#4C1E3C",
                "nombre": "vacia",
                "posicion": {
                    "x": 950,
                    "y": 50
                },
                "sillas": [
                    {
                        "color": "#8171BE",
                        "numero": 1,
                        "ocupada": false,
                        "posicion": {
                            "x": 930,
                            "y": 50
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 2,
                        "ocupada": false,
                        "posicion": {
                            "x": 1089,
                            "y": 19
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 3,
                        "ocupada": false,
                        "posicion": {
                            "x": 999,
                            "y": 7
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 4,
                        "ocupada": false,
                        "posicion": {
                            "x": 1149,
                            "y": 79
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 5,
                        "ocupada": false,
                        "posicion": {
                            "x": 1149,
                            "y": 169
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 6,
                        "ocupada": false,
                        "posicion": {
                            "x": 1109,
                            "y": 234
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 7,
                        "ocupada": false,
                        "posicion": {
                            "x": 1029,
                            "y": 254
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 8,
                        "ocupada": false,
                        "posicion": {
                            "x": 949,
                            "y": 229
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 9,
                        "ocupada": false,
                        "posicion": {
                            "x": 909,
                            "y": 169
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 10,
                        "ocupada": false,
                        "posicion": {
                            "x": 899,
                            "y": 109
                        }
                    },
                ]
            },
        
            {
                "color": "#4C1E3C",
                "nombre": "vacia",
                "posicion": {
                    "x": 50,
                    "y": 500
                },
                "sillas": [
                {
                        "color": "#8171BE",
                        "numero": 1,
                        "ocupada": false,
                        "posicion": {
                            "x": 30,
                            "y": 500
                    }
                },
        
                    {
                        "color": "#8171BE",
                        "numero": 2,
                        "ocupada": false,
                        "posicion": {
                            "x": 100,
                            "y": 460
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 3,
                        "ocupada": false,
                        "posicion": {
                            "x": 180,
                            "y": 460
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 4,
                        "ocupada": false,
                        "posicion": {
                            "x": 234,
                            "y": 500
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 5,
                        "ocupada": false,
                        "posicion": {
                            "x": 254,
                            "y": 569
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 6,
                        "ocupada": false,
                        "posicion": {
                            "x": 249,
                            "y": 634
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 7,
                        "ocupada": false,
                        "posicion": {
                            "x": 199,
                            "y": 684
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 8,
                        "ocupada": false,
                        "posicion": {
                            "x": 129,
                            "y": 704
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 9,
                        "ocupada": false,
                        "posicion": {
                            "x": 44,
                            "y": 669
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 10,
                        "ocupada": false,
                        "posicion": {
                            "x": 4,
                            "y": 599
                        }
                    }
                ]
            },
        
            {
                "color": "#4C1E3C",
                "nombre": "vacia",
                "posicion": {
                    "x": 500,
                    "y": 500
                },
                "sillas": [
                    {
                        "color": "#8171BE",
                        "numero": 1,
                        "ocupada": false,
                        "posicion": {
                            "x": 494,
                            "y": 490
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 2,
                        "ocupada": false,
                        "posicion": {
                            "x": 550,
                            "y": 456
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 3,
                        "ocupada": false,
                        "posicion": {
                            "x": 614,
                            "y": 459
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 4,
                        "ocupada": false,
                        "posicion": {
                            "x": 669,
                            "y": 490
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 5,
                        "ocupada": false,
                        "posicion": {
                            "x": 704,
                            "y": 554
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 6,
                        "ocupada": false,
                        "posicion": {
                            "x": 694,
                            "y": 634
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 7,
                        "ocupada": false,
                        "posicion": {
                            "x": 654,
                            "y": 684
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 8,
                        "ocupada": false,
                        "posicion": {
                            "x": 579,
                            "y": 704
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 9,
                        "ocupada": false,
                        "posicion": {
                            "x": 484,
                            "y": 664
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 10,
                        "ocupada": false,
                        "posicion": {
                            "x": 454,
                            "y": 579
                        }
                    },
                ]
            },
        
            {
                "color": "#4C1E3C",
                "nombre": "vacia",
                "posicion": {
                    "x": 900,
                    "y": 500
                },
                "sillas": [
                    {
                        "color": "#8171BE",
                        "numero": 1,
                        "ocupada": false,
                        "posicion": {
                            "x": 894,
                            "y": 490
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 2,
                        "ocupada": false,
                        "posicion": {
                            "x": 950,
                            "y": 456
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 3,
                        "ocupada": false,
                        "posicion": {
                            "x": 1014,
                            "y": 459
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 4,
                        "ocupada": false,
                        "posicion": {
                            "x": 1069,
                            "y": 490
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 5,
                        "ocupada": false,
                        "posicion": {
                            "x": 1104,
                            "y": 554
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 6,
                        "ocupada": false,
                        "posicion": {
                            "x": 1094,
                            "y": 634
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 7,
                        "ocupada": false,
                        "posicion": {
                            "x": 1054,
                            "y": 684
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 8,
                        "ocupada": false,
                        "posicion": {
                            "x": 979,
                            "y": 704
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 9,
                        "ocupada": false,
                        "posicion": {
                            "x": 884,
                            "y": 664
                        }
                    },
                    {
                        "color": "#8171BE",
                        "numero": 10,
                        "ocupada": false,
                        "posicion": {
                            "x": 854,
                            "y": 579
                        }
                    },
                ]
            }       
                ]
  constructor(mesas:MesasService) { 

        let ocupada=false;
        mesas.getmesas()
             .subscribe(data=>{
                this.mesas2=data;
                this.svgCanvas=document.getElementById("lienzo");
                let draw=SVG('lienzo').size(1500,1000);
                //console.log(data);
                let i=0;
                for(let mesa of this.mesas2 ){
                    //console.log(mesa);
                    let mesaF=draw.circle(200);
                    mesaF.attr({
                        fill:mesa.color
                });
                    let i=this.mesas2.indexOf(mesa);
                    mesaF.move(mesa.posicion.x,mesa.posicion.y); 
                    for(let silla of mesa.sillas){
                        let j=mesa.sillas.indexOf(silla);
                        let color=silla.ocupada?mesa.color:silla.color;
                        let sillaF=draw.circle(42).attr({
                        fill: color
                        });
                        sillaF.move(silla.posicion.x, silla.posicion.y);
                        //console.log("i: "+i+"j: "+j);
                        sillaF.click(function(){
                            if(silla.ocupada){
                                
                                alert('Silla Ocupada');

                            }
                            else{
                                console.log("vacia");
                            }
                        });
                        sillaF.click(function(){
                            console.log(mesa);
                            //data.silla.color=mesa.color;
                            //data.silla.ocupada=true;
                            ocupada=true;
                        /*
                       ;
                            mesas.actualizar(data)
                                 .subscribe(data=>{
                                     console.log(data);
                                 });
                                 */
                           
                        sillaF.attr({
                        fill:mesa.color
                        
                        });
                        
                        silla.ocupada=true;
                        console.log("i: " +i+" j: "+j);
                        
                        mesas.actualizar(silla,i-1,j-1)
                        .subscribe(res=>{
                            console.log(res);
                        });
                        
                        
                    });
                   
                    
                    j++;
                    }
                i++;
                }
                
                
             })
             
        
        //console.log("mesa4 "+this.mesas3);    
        /*     
        for(let mesa of this.mesas2){
            this.mesas3.push(mesa);
        }
        console.log("mesas3 "+this.mesas3);
          //console.log(JSON.stringify(this.mesas));
          */
        /*  
          mesas.ingresar(this.mesas)
                .subscribe(data=>{
                    console.log(data);
                });
        */        
    }


  ngOnInit() {
    /*this.svgCanvas=document.getElementById("lienzo");
    let draw=SVG('lienzo').size(1500,1000);
    

    for(let mesa of this.mesas2){
      console.log(mesa);
      let mesaF=draw.circle(200);
      mesaF.attr({
        fill:mesa.color
      });
      mesaF.move(mesa.posicion.x,mesa.posicion.y);
      for(let silla of mesa.sillas){
        let color=silla.ocupada?mesa.color:silla.color;
        let sillaF=draw.circle(30).attr({
          fill: color
        });
        sillaF.move(silla.posicion.x, silla.posicion.y);
        
       
        sillaF.click(function(){
        sillaF.attr({
          fill:mesa.color
        });
        silla.ocupada=true;
        console.log("silla");
       });
      }

    }
    */
  }

}
